using System;
using System.Collections.Generic;

namespace CsvLib
{
    public abstract class CsvBase
    {
        public CsvRow Header { get; protected set; } = new CsvRow();
        public string Footer { get; protected set; } = string.Empty;
        public char Delimiter { get; protected set; } = CsvDelimiter.Comma;
        protected string FileName { get; set; }
        protected bool HasHeader { get; set; }
        protected bool HasFooter { get; set; }
        protected IList<CsvRow> Rows { get; set; } = new List<CsvRow>();

        protected CsvBase(string fileName)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentException("fileName cannot be null or empty");
            }

            FileName = fileName;
        }

        protected CsvBase(string fileName, char delimiter, bool hasHeader, bool hasFooter = default)
        {
            if (string.IsNullOrWhiteSpace(fileName))
            {
                throw new ArgumentException("fileName cannot be null or empty");
            }

            FileName = fileName;
            Delimiter = delimiter;
            HasHeader = hasHeader;
            HasFooter = hasFooter;
        }
    }
}