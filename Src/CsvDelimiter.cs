﻿using System;

namespace CsvLib
{
    public static class CsvDelimiter
    {
        public static char Tab { get; } = '\t';
        public static char Space { get; } = ' ';
        public static char Comma { get; } = ',';
        public static char Colon { get; } = ';';
        public static char Pipe { get; } = '|';
    }
}