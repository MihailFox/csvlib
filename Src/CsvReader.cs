using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CsvLib
{
    public class CsvReader : CsvBase
    {
        public CsvReader(string fileName) : base(fileName)
        {
        }

        public CsvReader(string fileName, char delimiter, bool hasHeader, bool hasFooter = default) : base(fileName, delimiter, hasHeader, hasFooter)
        {
        }

        public IEnumerable<CsvRow> ReadAllRows()
        {
            try
            {
                var reader = File.ReadLines(FileName);

                if (reader?.Any() == false)
                {
                    throw new FileEmptyException($"{FileName} is empty");
                }

                foreach (var line in PopulateLines(reader))
                {
                    if (CsvRow.TryParse(line, Delimiter, out CsvRow csvRow))
                    {
                        Rows.Add(csvRow);
                    }
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine($"{ex.FileName} was not found");
                throw;
            }

            return Rows;
        }

        private IEnumerable<string> PopulateLines(IEnumerable<string> lines)
        {
            if (lines?.Any() != true)
            {
                throw new ArgumentException("lines cannot be null or empty");
            }

            if (HasHeader && HasFooter)
            {
                CsvRow.TryParse(lines?.First(), Delimiter, out CsvRow header);
                Header = header;
                Footer = lines?.Last().Trim();
                return lines.Skip(1).SkipLast(1);
            }
            else if (HasHeader)
            {
                CsvRow.TryParse(lines?.First(), Delimiter, out CsvRow header);
                Header = header;
                return lines.Skip(1);
            }
            else if (HasFooter)
            {
                Footer = lines?.Last().Trim();
                return lines.SkipLast(1);
            }
            else
            {
                return lines;
            }
        }
    }
}