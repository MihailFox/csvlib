﻿using System;

namespace CsvLib.Extensions
{
    public static class StringExtensions
    {
        public static string ToCamelCase(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentException("input cannot be null or empty");
            }

            var charArray = input.ToCharArray();
            charArray[0] = char.ToLowerInvariant(charArray[0]);

            return new string(charArray);
        }

        public static string AddQuotes(this string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentException("input cannot be null or empty");
            }

            return $@"""{input}""";
        }
    }
}