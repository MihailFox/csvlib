﻿using System;
using System.Text;

namespace CsvLib
{
    public static class CsvRowExtensions
    {
        public static string ToString(this CsvRow csvRow, char delimiter)
        {
            if (csvRow is null || csvRow.Count == 0)
            {
                throw new ArgumentException("csvRow cannot be null or empty");
            }
            var firstColumn = true;
            var builder = new StringBuilder();

            foreach (var field in csvRow)
            {
                if (!firstColumn)
                {
                    builder
                        .Append(delimiter)
                        .Append(CsvDelimiter.Space);
                }

                builder.Append(field);
                firstColumn = false;
            }
            return builder.ToString();
        }
    }
}