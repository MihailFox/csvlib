[![Maintainability](https://api.codeclimate.com/v1/badges/32f3915180b3169d379b/maintainability)](https://codeclimate.com/github/mihailfox/CsvLib/maintainability)
[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ea79c30f4a234428aa7d3bc0e876cefc)](https://www.codacy.com/manual/mihailfox/CsvLib?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=mihailfox/CsvLib&amp;utm_campaign=Badge_Grade)
# CsvLib
Simple C# library for dealing with CSV files
